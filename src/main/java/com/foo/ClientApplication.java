package com.foo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.protobuf.ProtobufHttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.foo.time.TimeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableScheduling
@EnableFeignClients
@EnableCircuitBreaker
@SpringBootApplication
public class ClientApplication implements CommandLineRunner {

	@Autowired
	private TimeService timeService;

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}

	@Bean
	public ProtobufHttpMessageConverter protobufHttpMessageConverter() {
		return new ProtobufHttpMessageConverter();
	}

	@Scheduled(initialDelay = 5000, fixedDelay = 5000)
	public void pollTimeService() {
		log.info(timeService.getTime().getTime());
	}

}
