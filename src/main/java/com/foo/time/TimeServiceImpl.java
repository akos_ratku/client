package com.foo.time;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foo.time.TimeProtos.Time;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired) )
public class TimeServiceImpl implements TimeService {

	private final RemoteTimeService remoteTimeService;

	@Override
	@HystrixCommand(fallbackMethod = "getTimeFallback")
	public Time getTime() {
		return remoteTimeService.getTime();
	}

	public Time getTimeFallback() {
		return Time.newBuilder().setTime("Failed to obtain time").build();
	}

}
