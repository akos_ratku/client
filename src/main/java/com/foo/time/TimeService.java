package com.foo.time;

import com.foo.time.TimeProtos.Time;

public interface TimeService {

	Time getTime();

}
