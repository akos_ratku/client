package com.foo.time;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.foo.time.TimeProtos.Time;

@FeignClient(url = "${timeservice.address}", name = "timeService")
public interface RemoteTimeService {

	@RequestMapping(value = "time", method = RequestMethod.GET, produces = "application/x-protobuf")
	Time getTime();

}
